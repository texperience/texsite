from setuptools import find_namespace_packages, setup


# Installation dependencies
install_requires = [
    "django>=3.2,<3.3",
    "django-bootstrap-ui>=2.0,<3.0",
    "wagtail>=4.1,<4.2",
]

# Development dependencies
linting_extras = [
    "brunette>=0.2,<1.0",
    "flake8>=6.0,<7.0",
    "isort>=5.11,<6.0",
]
testing_extras = [
    "coverage>=7.1,<8.0",
    "tox>=3.28,<4.0",
]
development_extras = linting_extras + testing_extras

setup(
    packages=find_namespace_packages(exclude=["tests"]),
    include_package_data=True,
    install_requires=install_requires,
    extras_require={
        "development": development_extras,
        "linting": linting_extras,
        "testing": testing_extras,
    },
)
