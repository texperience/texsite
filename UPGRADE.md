# Upgrade instructions

This document explains which steps you need to take in order to upgrade your texsite installation.

It is important that you follow the upgrade path, because of database migrations which must be applied using specific software versions:

2.0.0 > 3.0.0 > 4.0.0

Review the comments very focused as they highlight the relevant steps.

The concrete commands are examples and documented for a plain installation on a fictitious linux server.

## [4.5.0-dev]

## [4.4.0]
No actions needed.

## [4.3.0]
No actions needed.

## [4.2.0]
No actions needed.

## [4.1.0]
No actions needed.

## [4.0.0]
```bash
# Backup the data
tar cvfz $HOME/backup/texsite.`date +%Y-%m-%d`.tar.gz -C $TEXSITE_DATA_ROOT --transform 's,^./,,' .

# Due to changed dependencies rebuild the virtual environment
rmvirtualenv texsite-py38
mkvirtualenv --python=python3.8 texsite-py38

pip install texsite==4.0.0

# Migrate the database model
export DJANGO_SETTINGS_MODULE=texsite.settings.production
django-admin migrate
django-admin update_index
django-admin rebuild_references_index
django-admin remove_stale_contenttypes

# Collect the current version of static files
django-admin collectstatic

# Restart the web application
touch /var/www/texsite_wsgi.py

# Update your main and footer menus:
# ...edit the menu flags in the page promotion panel.
# ...sort the pages in the "sort menu order" page listing mode.
```

## [3.0.0]
```bash
# Add and activate the data root environment variable
cat << EOF >> ${HOME}/.bashrc
TEXSITE_DATA_ROOT=${HOME}/data/texsite
EOF

source ${HOME}/.bashrc

# Backup the data
tar cvfz $HOME/backup/texsite.`date +%Y-%m-%d`.tar.gz -C $TEXSITE_DATA_ROOT --transform 's,^./,,' .

# Due to changed dependencies rebuild the virtual environment
rmvirtualenv texsite-py38
mkvirtualenv --python=python3.8 texsite-py38

pip install texsite==3.0.0

# Migrate the database model
export DJANGO_SETTINGS_MODULE=texsite.settings.production
django-admin migrate
django-admin create_log_entries_from_revisions
django-admin fixtree --full

# Collect the current version of static files
django-admin collectstatic

# Restart the web application
touch /var/www/texsite_wsgi.py
```
