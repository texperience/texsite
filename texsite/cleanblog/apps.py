from django.apps import AppConfig


class TexsiteCleanBlogAppConfig(AppConfig):
    name = 'texsite.cleanblog'
    label = 'texsitecleanblog'
    verbose_name = 'texsite clean blog'
