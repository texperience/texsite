from django.apps import AppConfig


class TexsiteBusinessCasualAppConfig(AppConfig):
    name = 'texsite.businesscasual'
    label = 'texsitebusinesscasual'
    verbose_name = 'texsite business casual'
