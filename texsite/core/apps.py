from django.apps import AppConfig


class TexsiteCoreAppConfig(AppConfig):
    name = 'texsite.core'
    label = 'texsitecore'
    verbose_name = 'texsite core'
