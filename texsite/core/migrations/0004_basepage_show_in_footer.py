# Generated by Django 3.2.18 on 2023-02-26 11:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('texsitecore', '0003_purge_wagtailmenus'),
    ]

    operations = [
        migrations.AddField(
            model_name='basepage',
            name='show_in_footer',
            field=models.BooleanField(
                default=False,
                help_text='Whether a link to this page will appear in automatically generated footers',
                verbose_name='show in footer',
            ),
        ),
    ]
