from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0016_change_page_url_path_to_text_field'),
    ]

    operations = [
        migrations.CreateModel(
            name='BasePage',
            fields=[
                (
                    'page_ptr',
                    models.OneToOneField(
                        parent_link=True,
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        to='wagtailcore.Page',
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                'verbose_name': 'Base Page (texsite.core)',
            },
            bases=('wagtailcore.page',),
        ),
    ]
