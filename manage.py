import os
import sys


if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "texsite.settings.dev")
    os.environ.setdefault("TEXSITE_DATA_ROOT", ".")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
