from django.conf.urls import include, url
from wagtail import urls


urlpatterns = [
    url(r'', include(urls)),
]
