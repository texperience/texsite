FROM python:3.11-slim-buster

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /texsite
RUN pip install --root-user-action=ignore --upgrade pip
RUN pip install --root-user-action=ignore gunicorn

COPY . .
RUN pip install --root-user-action=ignore .
