# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][keepachangelog], and this project adheres to [Semantic Versioning][semver].

## [4.5.0-dev] - Unreleased

## [4.4.0] - 2023-10-16
### Added
* Highlight inline code in paragraphs

### Changed
* Improve page and article navigation responsiveness

## [4.3.0] - 2023-10-14
### Changed
* Sort articles by published date descending

### Fixed
* Fallback to author username for unset author name
* Show article published date on article list correctly

## [4.2.0] - 2023-10-14
### Added
* Build and deploy docker multi-arch images
* Insert, highlight and copy code snippets in articles
* Provide texperience favicons

### Fixed
* Show article author on article list correctly

## [4.1.0] - 2023-02-27
### Added
* Build and run entrypoints as docker containers
* Deploy docker images on CI build
* Compose local testing and development containers

## [4.0.0] - 2023-02-26
### Added
* Add support for Python 3.9, 3.10 and 3.11

### Changed
* Upgrade to Wagtail 4.1 LTS
* Upgrade to Django 3.2 LTS
* Configure menu appearance in page promote panel

## [3.0.0] - 2023-02-26
### Added
* Add application runtime settings and configuration
* Provide default error pages

### Changed
* Upgrade development dependencies
* Upgrade to Wagtail 2.11 LTS
* Upgrade to Django 3.1

### Removed
* Remove support for Python 3.6 and 3.7

## [2.0.0] - 2020-02-09
### Added
* Added Wagtail 2.7 support
* Added Django 2.2 support
* Added Python 3.7 and 3.8 support
* Initialise continuous integration with gitlab
* Add coverage output after tests

### Changed
* Pin testing dependencies according semantic versioning
* Upgrade direct dependencies wagtailmenus and django-bootstrap-ui
* Move project to gitlab

### Removed
* Remove Python 3.4 and 3.5 support
* Removed Wagtail 2.3 support

## [1.0.1] - 2018-11-10
### Added
* Released first stable version
* Added password protection template

### Changed
* Moved to 1.0.1 (aka 1.0.0) because of erroneous created tag and irreversible PyPI release

## [0.2.0] - 2018-11-04
### Added
* Added Wagtail 2.3 support
* Added Django 1.11 support
* Added Python 3.6 support

### Changed
* Moved copyright year for better readability
* Upgraded to wagtailmenus 2.10
* Upgraded to django-bootstrap-ui 0.5

### Removed
* Removed Wagtail 1.9 support
* Removed Django 1.8, 1.9 and 1.10 support
* Removed Python 2.7 and 3.3 support

## [0.1.0] - 2017-04-22
### Added
* Implemented Clean Blog app
* Implemented Business Casual app
* Added Wagtail 1.9 support
* Added Django 1.10 support

### Removed
* Removed Wagtail 1.2 support
* Removed Django 1.7 support

## [0.0.2] - 2015-12-14
### Added
* Added Django 1.9 support

### Changed
* Optimized automated pypi deployment

## [0.0.1] - 2015-12-14
### Added
* Initial release
* Test automated pypi deployment
* Added Wagtail 1.2 support
* Added Django 1.7 support
* Added Django 1.8 support

[keepachangelog]: https://keepachangelog.com/en/1.0.0/
[semver]: https://semver.org/spec/v2.0.0.html
