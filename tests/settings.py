DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'testdb.sqlite3',
    }
}
INSTALLED_APPS = [
    # texsite test apps
    'tests.businesscasual',
    'tests.cleanblog',
    'tests.core',
    # texsite apps
    'texsite.businesscasual',
    'texsite.cleanblog',
    'texsite.core',
    # texperience apps
    'bootstrap_ui',
    # Wagtail apps
    'wagtail.users',
    'wagtail.documents',
    'wagtail.images',
    'wagtail.admin',
    'wagtail',
    # Third party apps
    'taggit',
    # Django apps
    'django.contrib.auth',
    'django.contrib.contenttypes',
]
MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
]
ROOT_URLCONF = 'tests.urls'
SECRET_KEY = 'test-key'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
            'libraries': {
                'bootstrap_ui_tags': 'bootstrap_ui.templatetags.bootstrap_ui_tags',
            },
        },
    },
]
